package com.example.recetas2;

public class Receta {
    private String titulo;
    private String grafico;
    private String ingredientes;
    private String preparacion;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGrafico() {
        return grafico;
    }

    public void setGrafico(String grafico) {
        this.grafico = grafico;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getPreparacion() {
        return preparacion;
    }

    public void setPreparacion(String preparacion) {
        this.preparacion = preparacion;
    }

    @Override
    public String toString() {
        return "Receta{" +
                "titulo='" + titulo + '\'' +
                ", grafico=" + grafico +
                ", ingredientes='" + ingredientes + '\'' +
                ", preparacion='" + preparacion + '\'' +
                '}';
    }
}
