package com.example.recetas2;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Receta receta;
    String contenidoIngredientes = "";
    String contenidoPreparacion = "";
    public final static List<Receta> recetas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (recetas.size() == 0)
            leerArchivo();

        final ListView listView = (ListView) findViewById(R.id.listaRecetas);
        List titulos = new ArrayList();
        for (Receta receta1 : recetas) {
            titulos.add(receta1.getTitulo());
        }

        ArrayList<String> titulosList = new ArrayList<String>();
        titulosList.addAll(titulos);

        // Create ArrayAdapter using the planet list.
        ArrayAdapter listAdapter = new ArrayAdapter<String>(this, R.layout.row_recetario, titulosList);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition = position;

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);
                cambiarLayout(itemPosition);
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });
    }

    public void cambiarLayout(int posicion) {
        Intent nv = new Intent(this, RecetaActivity.class);
        nv.putExtra("posicion", posicion);
        startActivity(nv);
        // Show Alert
    }

    public void leerArchivo() {
        String estado = Environment.getExternalStorageState();
        if (!estado.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "No hay memoria SD", Toast.LENGTH_LONG).show();
            finish();
        }
        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            File dir = Environment.getExternalStorageDirectory();

            File pt = new File(dir.getAbsolutePath() + File.separator + "recetas.txt");
            BufferedReader lee = new BufferedReader(new FileReader(pt));
            String linea, linea1;
            Boolean bIngredientes = false;
            Boolean bPreparacion = false;


            while ((linea = lee.readLine()) != null) {

                if (linea.contains("Titulo:")) {
                    if (receta != null) {
                        receta.setPreparacion(contenidoPreparacion);
                        recetas.add(receta);
                    }
                    contenidoIngredientes = "";
                    contenidoPreparacion = "";
                    bPreparacion = false;
                    bIngredientes = false;
                    receta = new Receta();
                    receta.setTitulo(linea.split(":")[1]);
                }

                if (linea.contains("Grafico:")) {
                    receta.setGrafico(linea.split(":")[1]);
                }
                if (linea.contains("Ingredientes:")) {

                    contenidoIngredientes = "";
                    bIngredientes = true;
                }

                if (linea.contains("Preparacion:")) {
                    receta.setIngredientes(contenidoIngredientes);
                    bIngredientes = false;
                    contenidoPreparacion = "";
                    bPreparacion = true;
                }

                if (bIngredientes) {
                    contenidoIngredientes += linea + "\n";

                }
                if (bPreparacion) {
                    contenidoPreparacion += linea + "\n";
                }


            }
            if (receta != null) {
                receta.setPreparacion(contenidoPreparacion);
                recetas.add(receta);
            }

        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
