package com.example.recetas2;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import static com.example.recetas2.MainActivity.recetas;

public class RecetaActivity extends AppCompatActivity {

    private Receta recetas1;
    private TextView titulo, ingredientes, preparacion;
    private ImageView imagen;
    private int images[] = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_receta);
        titulo = (TextView) findViewById(R.id.titulo);
        ingredientes = (TextView) findViewById(R.id.contenidoIngrediente);
        preparacion = (TextView) findViewById(R.id.contenidoPreparacion);
        imagen = (ImageView) findViewById(R.id.imageReceta);

        Bundle p = getIntent().getExtras();
        int posicion = p.getInt("posicion");
        recetas1 = recetas.get(posicion);

        titulo.setText(recetas1.getTitulo());
        ingredientes.setText(recetas1.getIngredientes());
        preparacion.setText(recetas1.getPreparacion());
        imagen.setImageResource(images[posicion]);

    }
}
