package com.example.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText peso;
    EditText altura ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        peso = (EditText) findViewById(R.id.edit_peso);
        altura = (EditText) findViewById(R.id.edit_altura);
    }

    public void calcularImc(View view) {
        if (peso != null && altura != null &&  Double.parseDouble(peso.getText().toString()) != 0.0 && Double.parseDouble(altura.getText().toString()) != 0.0) {

        }
        Intent nv = new Intent(this, ImcActivity.class);
        nv.putExtra("peso", Double.parseDouble(peso.getText().toString()));
        nv.putExtra("altura", Double.parseDouble(altura.getText().toString()));
        nv.putExtra("nombre", "Dario");
        startActivity(nv);
    }
}
