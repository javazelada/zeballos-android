package com.example.imcapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ImcActivity extends AppCompatActivity {
    private TextView mostrarDato;
    private ImageView imagen;
    private Double imcDato, qpeso, qaltura;
    private String qnombre;
    private int pesos[] = {R.drawable.menor18, R.drawable.entre18a24, R.drawable.entre25a29, R.drawable.entre30a34,
            R.drawable.entre35a39, R.drawable.entre40a49, R.drawable.mayor50};
    private String pesosTitulo[] = {"Bajo peso", "Normal", "SobrePeso", "Obesidad I", "Obesidad II", "Obesidad III", "Obesidad IV"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);
        Bundle p = getIntent().getExtras();
        mostrarDato = (TextView) findViewById(R.id.datoPeso);
        imagen = (ImageView) findViewById(R.id.imagen);
        qnombre = p.getString("nombre");
        qpeso = p.getDouble("peso");
        qaltura = p.getDouble("altura");
        calcularImc();

    }

    public void calcularImc() {
        imcDato = qpeso / (qaltura * qaltura);
        if (imcDato < 18.5) {
            mostrarDato.setText(pesosTitulo[0]);
            imagen.setImageResource(pesos[0]);

        }
        if (imcDato >= 18.5 && imcDato < 24.9) {
            mostrarDato.setText(pesosTitulo[1]);
            imagen.setImageResource(pesos[1]);
        }
        if (imcDato >= 25 && imcDato < 29.9) {
            mostrarDato.setText(pesosTitulo[2]);
            imagen.setImageResource(pesos[2]);
        }
        if (imcDato >= 30 && imcDato < 34.9) {
            mostrarDato.setText(pesosTitulo[3]);
            imagen.setImageResource(pesos[3]);
        }
        if (imcDato >= 35 && imcDato < 39.9) {
            mostrarDato.setText(pesosTitulo[4]);
            imagen.setImageResource(pesos[4]);
        }
        if (imcDato >= 40 && imcDato < 49.9) {
            mostrarDato.setText(pesosTitulo[5]);
            imagen.setImageResource(pesos[5]);
        }
        if (imcDato >= 50) {
            mostrarDato.setText(pesosTitulo[6]);
            imagen.setImageResource(pesos[6]);
        }

    }

    public void retornar(View view) {
        Intent nv = new Intent(this, MainActivity.class);
        startActivity(nv);
        qpeso = 0.0;
        qaltura = 0.0;
    }


}
