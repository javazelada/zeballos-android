package com.example.archivos2;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static List<Persona> personas = new ArrayList<>();
    public static List<Aportes> aportes = new ArrayList<>();
    public static List<Aportes> aportePersona = new ArrayList<>();
    private String carnett;
    private int montoAportes;
    Persona p;
    Aportes aporte;
    public TextView busquedaCi, nombres, paterno, materno, telefono, montoTotal, nroAportes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        busquedaCi = (EditText) findViewById(R.id.editTextCarnet);
        nombres = (TextView) findViewById(R.id.editTextNombres);
        paterno = (TextView) findViewById(R.id.editTextPaterno);
        materno = (TextView) findViewById(R.id.editTextMaterno);
        telefono = (TextView) findViewById(R.id.editTextTelefono);
        nroAportes = (TextView) findViewById(R.id.nroAportes);
        montoTotal = (TextView) findViewById(R.id.totalMonto);

        inicializar();
        if (personas.size() == 0)
            leerArchivo();


    }

    public void leerArchivo() {
        String estado = Environment.getExternalStorageState();
        if (!estado.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "No hay memoria SD", Toast.LENGTH_LONG).show();
            finish();
        }
        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            File dir = Environment.getExternalStorageDirectory();

            File pt = new File(dir.getAbsolutePath() + File.separator + "bdPersonal.csv");
            BufferedReader lee = new BufferedReader(new FileReader(pt));
            String linea, linea1;

            File pt1 = new File(dir.getAbsolutePath() + File.separator + "bdAportes.csv");
            BufferedReader lee1 = new BufferedReader(new FileReader(pt1));

            while ((linea = lee.readLine()) != null) {
                p = new Persona();
                p.setPaterno(linea.split(";")[0]);
                p.setMaterno(linea.split(";")[1]);
                p.setNombres(linea.split(";")[2]);
                p.setTelefono(linea.split(";")[3]);
                p.setCi(linea.split(";")[4]);
                personas.add(p);
            }

            while ((linea1 = lee1.readLine()) != null) {
                String aporteString = "";
                Integer aporteInt = 0;
                aporte = new Aportes();
                aporte.setCarnet(linea1.split(";")[0]);
                aporteString = linea1.split(";")[1];
                try {
                    aporteInt = Integer.parseInt(aporteString);
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                aporte.setAporte(aporteInt);
                aporte.setCat(linea1.split(";")[2]);
                aportes.add(aporte);
            }
            Log.d("aportes", "lista aportess: " + aportes);


        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void buscarCarnet(View view) {
        for (Persona per : personas) {
            if (per.getCi().equals(busquedaCi.getText().toString())) {
                carnett = per.getCi();
                nombres.setText(per.getNombres());
                paterno.setText(per.getPaterno());
                materno.setText(per.getMaterno());
                telefono.setText(per.getTelefono());

            }
        }
        buscarAportes();
    }

    public void inicializar() {
//        personas.clear();
//        aportes.clear();
        aportePersona.clear();
        carnett = "";
        montoAportes = 0;
        busquedaCi.setText("");
        nombres.setText("");
        paterno.setText("");
        materno.setText("");
        telefono.setText("");
        montoTotal.setText("");
        nroAportes.setText("");
    }

    public void iniciar(View view) {
        inicializar();
    }

    public void buscarAportes() {
        for (Aportes aporte : aportes) {
            if (aporte.getCarnet().equals(carnett)) {
                montoAportes += aporte.getAporte();
                aportePersona.add(aporte);
            }
        }
        nroAportes.setText(aportePersona.size() + "");
        montoTotal.setText(montoAportes + "");


    }

    public void buscar(View view) {
        Intent nv = new Intent(this, busquedaActivity.class);
        startActivity(nv);
    }

    public void detalle(View view) {
        Intent nv = new Intent(this, detalleAportes.class);
        startActivity(nv);
    }
}
