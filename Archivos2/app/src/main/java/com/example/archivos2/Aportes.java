package com.example.archivos2;

public class Aportes {
    private String carnet;
    private int aporte;
    private String cat;

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public int getAporte() {
        return aporte;
    }

    public void setAporte(int aporte) {
        this.aporte = aporte;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    @Override
    public String toString() {
        return "Aportes{" +
                "carnet='" + carnet + '\'' +
                ", aporte=" + aporte +
                ", cat='" + cat + '\'' +
                '}';
    }
}
