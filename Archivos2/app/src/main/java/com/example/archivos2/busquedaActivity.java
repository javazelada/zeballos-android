package com.example.archivos2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import static com.example.archivos2.MainActivity.personas;

public class busquedaActivity extends AppCompatActivity {
    public List<String> coincidencias = new ArrayList<>();
    EditText busqueda;
    TextView coincidencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        busqueda = (EditText) findViewById(R.id.TextBusqueda2);
        coincidencia = (TextView) findViewById(R.id.coincidencias);
    }

    public void buscarTexto(View view) {
        encontrarPalabra(busqueda.getText().toString());

    }

    public void encontrarPalabra(String palabra) {
        String textoCoincidencia = "";
        Log.d("busqueda", "busqueda nombres string:" + palabra);
        for (Persona persona : personas) {
            if (persona.getPaterno().toLowerCase().contains(palabra.toLowerCase())){
                coincidencias.add(persona.getPaterno());
                textoCoincidencia+= persona.getPaterno() + ", ";
            }
            if (persona.getMaterno().toLowerCase().contains(palabra.toLowerCase())) {
                coincidencias.add(persona.getMaterno());
                textoCoincidencia+= persona.getMaterno() + ", ";
            }
            if (persona.getNombres().toLowerCase().contains(palabra.toLowerCase())) {
                coincidencias.add(persona.getNombres());
                textoCoincidencia+= persona.getNombres() + ", ";
            }
        }
        coincidencia.setText(textoCoincidencia);

        Log.d("busquda", "busqueda nombres: " + coincidencias);
    }

}
