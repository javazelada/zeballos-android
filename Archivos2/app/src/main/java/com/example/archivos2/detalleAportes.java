package com.example.archivos2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import static com.example.archivos2.MainActivity.aportePersona;

public class detalleAportes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_aportes);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        Log.d("recycler", "rrrrr f: " + aportePersona);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        Adaptador myAdapter = new Adaptador(aportePersona);
        recyclerView.setAdapter(myAdapter);
    }
}
