package com.example.conversiondemonedas;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.conversor);

    }

    public void procesar(View vista) {
        TextView p1 = (TextView) findViewById(R.id.conv_brazil);
        TextView p2 = (TextView) findViewById(R.id.conv_chile);
        TextView p3 = (TextView) findViewById(R.id.conv_dolares);
        TextView p1 = (TextView) findViewById(R.id.conv_euro);
        TextView p2 = (TextView) findViewById(R.id.conv_japon);

        String vs1 = p1.getText().toString();
        String vs2 = p2.getText().toString();

        int vBase = Integer.parseInt(vs1);
        int vAltura = Integer.parseInt(vs2);

        int area = vBase * vAltura;

        p3.setText(area + "");

    }
}
