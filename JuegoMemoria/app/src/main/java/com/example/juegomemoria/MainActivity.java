package com.example.juegomemoria;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private ImageButton imgb1, imgb2, imgb3, imgb4, imgb5, imgb6, imgb7, imgb8, imgb9, imgb10, imgb11, imgb12;
    public List<Integer> v = new ArrayList<>();

    private Integer imgMemoria[] = {
            R.drawable.memocharlie, R.drawable.memocharlie,
            R.drawable.memomarcia, R.drawable.memomarcia,
            R.drawable.memorerun, R.drawable.memorerun,
            R.drawable.memosally, R.drawable.memosally,
            R.drawable.memoschroeder, R.drawable.memoschroeder,
            R.drawable.memosnoopy, R.drawable.memosnoopy};

    private int pulsaciones = 0;

    private Boolean imgPosicicion[] = {false, false, false, false, false, false,
            false, false, false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgb1 = (ImageButton) findViewById(R.id.imageButton1);
        imgb2 = (ImageButton) findViewById(R.id.imageButton2);
        imgb3 = (ImageButton) findViewById(R.id.imageButton3);
        imgb4 = (ImageButton) findViewById(R.id.imageButton4);
        imgb5 = (ImageButton) findViewById(R.id.imageButton5);
        imgb6 = (ImageButton) findViewById(R.id.imageButton6);
        imgb7 = (ImageButton) findViewById(R.id.imageButton7);
        imgb8 = (ImageButton) findViewById(R.id.imageButton8);
        imgb9 = (ImageButton) findViewById(R.id.imageButton9);
        imgb10 = (ImageButton) findViewById(R.id.imageButton10);
        imgb11 = (ImageButton) findViewById(R.id.imageButton11);
        imgb12 = (ImageButton) findViewById(R.id.imageButton12);
        desordenarImagen();
        //mostrarImagenes();
        ponerEnBlancoTodo();

    }

    public void inicializar(View view) {
//        ponerEnBlancoTodo();
        for (int i = 0; i < imgPosicicion.length; i++) {
            imgPosicicion[i] = false;
        }
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void ponerEnBlancoTodo() {
        imgb1.setImageResource(R.drawable.memocara);
        imgb2.setImageResource(R.drawable.memocara);
        imgb3.setImageResource(R.drawable.memocara);
        imgb4.setImageResource(R.drawable.memocara);
        imgb5.setImageResource(R.drawable.memocara);
        imgb6.setImageResource(R.drawable.memocara);
        imgb7.setImageResource(R.drawable.memocara);
        imgb8.setImageResource(R.drawable.memocara);
        imgb9.setImageResource(R.drawable.memocara);
        imgb10.setImageResource(R.drawable.memocara);
        imgb11.setImageResource(R.drawable.memocara);
        imgb12.setImageResource(R.drawable.memocara);
    }

    private void desordenarImagen() {
        List<Integer> lista = Arrays.asList(imgMemoria);
        Collections.shuffle(lista);
        imgMemoria = lista.toArray(imgMemoria);
    }

    private void mostrarImagenes() {
        imgb1.setImageResource(imgMemoria[0]);
        imgb2.setImageResource(imgMemoria[1]);
        imgb3.setImageResource(imgMemoria[2]);
        imgb4.setImageResource(imgMemoria[3]);
        imgb5.setImageResource(imgMemoria[4]);
        imgb6.setImageResource(imgMemoria[5]);
        imgb7.setImageResource(imgMemoria[6]);
        imgb8.setImageResource(imgMemoria[7]);
        imgb9.setImageResource(imgMemoria[8]);
        imgb10.setImageResource(imgMemoria[9]);
        imgb11.setImageResource(imgMemoria[10]);
        imgb12.setImageResource(imgMemoria[11]);
//        imgb12.setBackgroundResource(R.drawable.memocara);
        Log.d("vectorImg", "imagenes" + Arrays.asList(imgMemoria));
    }

    public void verificar(View view) {
        if (v != null && v.size() >= 2) {
//            if (v.size() >= 2) {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                }
//            }
            v.clear();
        }
        switch (view.getId()) {
            case R.id.imageButton1:

                imgb1.setImageResource(imgMemoria[0]);
                imgb1.setEnabled(false);
                v.add(0);
                break;
            case R.id.imageButton2:
                imgb2.setImageResource(imgMemoria[1]);
                imgb2.setEnabled(false);
                v.add(1);
                break;
            case R.id.imageButton3:
                imgb3.setImageResource(imgMemoria[2]);
                imgb3.setEnabled(false);
                v.add(2);
                break;
            case R.id.imageButton4:
                imgb4.setImageResource(imgMemoria[3]);
                imgb4.setEnabled(false);
                v.add(3);
                break;
            case R.id.imageButton5:
                imgb5.setImageResource(imgMemoria[4]);
                imgb5.setEnabled(false);
                v.add(4);
                break;
            case R.id.imageButton6:
                imgb6.setImageResource(imgMemoria[5]);
                imgb6.setEnabled(false);
                v.add(5);
                break;
            case R.id.imageButton7:
                imgb7.setImageResource(imgMemoria[6]);
                imgb7.setEnabled(false);
                v.add(6);
                break;
            case R.id.imageButton8:
                imgb8.setImageResource(imgMemoria[7]);
                imgb8.setEnabled(false);
                v.add(7);
                break;
            case R.id.imageButton9:
                imgb9.setImageResource(imgMemoria[8]);
                imgb9.setEnabled(false);
                v.add(8);
                break;
            case R.id.imageButton10:
                imgb10.setImageResource(imgMemoria[9]);
                imgb10.setEnabled(false);
                v.add(9);
                break;
            case R.id.imageButton11:
                imgb11.setImageResource(imgMemoria[10]);
                imgb11.setEnabled(false);
                v.add(10);
                break;
            case R.id.imageButton12:
                imgb12.setImageResource(imgMemoria[11]);
                imgb12.setEnabled(false);
                v.add(11);
                break;
        }

        if (v.size() == 2) {
            Log.d("pushh", "hhhhh111 -> " + v.size()+ " , " + imgMemoria[v.get(0)] +  "  ===  "+  imgMemoria[v.get(1)] );

            if (imgMemoria[v.get(0)].equals(imgMemoria[v.get(1)]) ){
                imgPosicicion[v.get(0)] = true;
                imgPosicicion[v.get(1)] = true;

                imagenCorrecta(v);

            }
            limpiarCaras();


        }
        verificarVictoria();


    }

    public void verificarVictoria() {
        int or = 0;
        for (int i = 0; i < imgPosicicion.length; i++) {
            if (imgPosicicion[i]) {
                or ++;
            }
        }
        if (or == 12) {
            Context context = getApplicationContext();
            CharSequence text = "Ganaste!";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }

    }

    public void limpiarCaras() {
        ScheduledExecutorService scheduler
                = Executors.newSingleThreadScheduledExecutor();

        Runnable task = new Runnable() {
            public void run() {
                for (int i = 0; i < imgPosicicion.length; i++) {
                    Log.d("i3334", "valores: " + !imgPosicicion[i]);

                    if (!imgPosicicion[i]) {

                        switch (i) {
                            case 0:
                                imgb1.setImageResource(R.drawable.memocara);
                                imgb1.setEnabled(true);
                                break;
                            case 1:
                                imgb2.setImageResource(R.drawable.memocara);
                                imgb2.setEnabled(true);
                                break;
                            case 2:
                                imgb3.setImageResource(R.drawable.memocara);
                                imgb3.setEnabled(true);
                                break;
                            case 3:
                                imgb4.setImageResource(R.drawable.memocara);
                                imgb4.setEnabled(true);
                                break;
                            case 4:
                                imgb5.setImageResource(R.drawable.memocara);
                                imgb5.setEnabled(true);
                                break;
                            case 5:
                                imgb6.setImageResource(R.drawable.memocara);
                                imgb6.setEnabled(true);
                                break;
                            case 6:
                                imgb7.setImageResource(R.drawable.memocara);
                                imgb7.setEnabled(true);
                                break;
                            case 7:
                                imgb8.setImageResource(R.drawable.memocara);
                                imgb8.setEnabled(true);
                                break;
                            case 8:
                                imgb9.setImageResource(R.drawable.memocara);
                                imgb9.setEnabled(true);
                                break;
                            case 9:
                                imgb10.setImageResource(R.drawable.memocara);
                                imgb10.setEnabled(true);
                                break;
                            case 10:
                                imgb11.setImageResource(R.drawable.memocara);
                                imgb11.setEnabled(true);
                                break;
                            case 11:
                                imgb12.setImageResource(R.drawable.memocara);
                                imgb12.setEnabled(true);
                                break;
                        }
                    }
                }
            }
        };

        int delay = 100;
        scheduler.schedule(task, delay, TimeUnit.MILLISECONDS);
        scheduler.shutdown();

    }

    private void imagenCorrecta(List<Integer> v) {
        for (int i : v) {
            switch (i) {
                case 0:
                    imgb1.setBackgroundResource(0);
                    break;
                case 1:
                    imgb2.setBackgroundResource(0);
                    break;
                case 2:
                    imgb3.setBackgroundResource(0);
                    break;
                case 3:
                    imgb4.setBackgroundResource(0);
                    break;
                case 4:
                    imgb5.setBackgroundResource(0);
                    break;
                case 5:
                    imgb6.setBackgroundResource(0);
                    break;
                case 6:
                    imgb7.setBackgroundResource(0);
                    break;
                case 7:
                    imgb8.setBackgroundResource(0);
                    break;
                case 8:
                    imgb9.setBackgroundResource(0);
                    break;
                case 9:
                    imgb10.setBackgroundResource(0);
                    break;
                case 10:
                    imgb11.setBackgroundResource(0);
                    break;
                case 11:
                    imgb12.setBackgroundResource(0);
                    break;
            }
        }

    }
}

//setbackground resources
