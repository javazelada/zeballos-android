package com.example.archivosprecio.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.archivosprecio.Precio;
import com.example.archivosprecio.Producto;

@Database(entities = {Precio.class, Producto.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PrecioDao precioDao();
    public abstract ProductoDao productoDao();
}