package com.example.archivosprecio.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.archivosprecio.Precio;

import java.util.List;

@Dao
public interface PrecioDao {
    @Query("SELECT * FROM precio")
    List<Precio> getAll();

    @Insert
    void insertAll(List<Precio> precios);

    @Delete
    void delete(Precio precio);
}
