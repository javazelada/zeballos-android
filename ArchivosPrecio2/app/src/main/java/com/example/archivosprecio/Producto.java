package com.example.archivosprecio;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Producto {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int codigo;
    private String producto;
    private String univen;
    private int uniem;
    private int linea;
    private int exitencia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getUniven() {
        return univen;
    }

    public void setUniven(String univen) {
        this.univen = univen;
    }

    public int getUniem() {
        return uniem;
    }

    public void setUniem(int uniem) {
        this.uniem = uniem;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public int getExitencia() {
        return exitencia;
    }

    public void setExitencia(int exitencia) {
        this.exitencia = exitencia;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "id=" + id +
                ", codigo=" + codigo +
                ", producto='" + producto + '\'' +
                ", univen='" + univen + '\'' +
                ", uniem=" + uniem +
                ", linea=" + linea +
                ", exitencia=" + exitencia +
                '}';
    }
}
