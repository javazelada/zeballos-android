package com.example.archivosprecio;

import android.Manifest;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import com.example.archivosprecio.db.AppDatabase;
import com.example.archivosprecio.db.PrecioDao;
import com.example.archivosprecio.db.ProductoDao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConsultaActivity extends AppCompatActivity {

    public static List<Producto> productos = new ArrayList<>();
    public static List<Precio> precios = new ArrayList<>();
    private PrecioDao precioDao;
    private ProductoDao productoDao;
    private Producto p;
    private Precio precio;
    private EditText busqueda;
    private TextView descripcion, precioVenta, univen, unixenv, codLin, existencia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
    }

    public void inicializardb(View view) {
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "listaPrecios-db").allowMainThreadQueries().build();
        db.clearAllTables();
        precioDao = db.precioDao();
        productoDao = db.productoDao();

    }

    public void migrar(View view) {
        leerArchivo();

    }

    public void consultatexto(View view) {

    }

    public void consultadb(View view) {

    }

    public void finalizar(View view) {

    }

    public void leerArchivo() {
        String estado = Environment.getExternalStorageState();
        if (!estado.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "No hay memoria SD", Toast.LENGTH_LONG).show();
            finish();
        }
        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            File dir = Environment.getExternalStorageDirectory();

            File pt = new File(dir.getAbsolutePath() + File.separator + "productos.csv");
            BufferedReader lee = new BufferedReader(new FileReader(pt));
            String linea, linea1;

            File pt1 = new File(dir.getAbsolutePath() + File.separator + "precios.csv");
            BufferedReader lee1 = new BufferedReader(new FileReader(pt1));

            while ((linea = lee.readLine()) != null) {
                p = new Producto();
                try {
                    p.setCodigo(Integer.parseInt(linea.split(";")[0]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                p.setProducto(linea.split(";")[1]);
                p.setUniven(linea.split(";")[2]);
                try {
                    p.setUniem(Integer.parseInt(linea.split(";")[3]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                try {
                    p.setLinea(Integer.parseInt(linea.split(";")[4]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                try {
                    p.setExitencia(Integer.parseInt(linea.split(";")[5]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                productos.add(p);

            }

            while ((linea1 = lee1.readLine()) != null) {
                precio = new Precio();
                try {
                    precio.setCodigo(Integer.parseInt(linea1.split(";")[0]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                try {
                    Date data1 = new SimpleDateFormat("dd/MM/yyyy").parse(linea1.split(";")[2]);
                    precio.setFecha(data1.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    precio.setPrecio(Double.parseDouble(linea1.split(";")[3]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                precios.add(precio);
            }

        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        productoDao.insertAll(productos);
        precioDao.insertAll(precios);

    }

}
