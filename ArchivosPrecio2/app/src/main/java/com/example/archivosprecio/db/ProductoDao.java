package com.example.archivosprecio.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.archivosprecio.Producto;

import java.util.List;

@Dao
public interface ProductoDao {
    @Query("SELECT * FROM producto")
    List<Producto> getAll();

    @Insert
    void insertAll(List<Producto> productos);

    @Delete
    void delete(Producto producto);
}
