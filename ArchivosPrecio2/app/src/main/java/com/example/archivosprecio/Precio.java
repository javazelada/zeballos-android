package com.example.archivosprecio;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Precio {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int codigo;
    private Long fecha;
    private double precio;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Precio{" +
                "id=" + id +
                ", codigo=" + codigo +
                ", fecha=" + fecha +
                ", precio=" + precio +
                '}';
    }
}
