package com.example.productos;

import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String estado = Environment.getExternalStorageState();
        if (!estado.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "No hay memoria SD", Toast.LENGTH_LONG).show();
            finish();
        }
        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            File dir = Environment.getExternalStorageDirectory();
            File pt = new File(dir.getAbsolutePath() + File.separator + "precios.csv");
            BufferedReader lee = new BufferedReader(new FileReader(pt));
            String linea, res = "", lt;
            int j;
            double total = 0;
            double bb[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            String tipos[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
            while ((linea = lee.readLine()) != null) {
                res = linea.split(";")[3];
                total = Double.parseDouble(res);
                lt = linea.split(";")[1];
                char[] c = lt.toCharArray();
                j = (int) c[0] - 65;
                bb[j] = bb[j] + total;
            }
            res = "";
            for (int i = 0; i < 9; i++) {
                res = res + tipos[i] + " = " + bb[i] + "\n";
            }
            TextView qt = (TextView) findViewById(R.id.text1);
            qt.setText(res);
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void finalizar(View vista) {
        finish();
    }
}
