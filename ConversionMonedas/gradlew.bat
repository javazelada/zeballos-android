<?xml version="1.0" encoding="UTF-8"?>
<root>
  <item name="androidx.print.PrintHelper android.graphics.Bitmap convertBitmapForColorMode(android.graphics.Bitmap, int) 1">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.COLOR_MODE_MONOCHROME, androidx.print.PrintHelper.COLOR_MODE_COLOR}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper android.graphics.Matrix getMatrix(int, int, android.graphics.RectF, int) 3">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.SCALE_MODE_FIT, androidx.print.PrintHelper.SCALE_MODE_FILL}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper int getColorMode()">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.COLOR_MODE_MONOCHROME, androidx.print.PrintHelper.COLOR_MODE_COLOR}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper int getScaleMode()">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.SCALE_MODE_FIT, androidx.print.PrintHelper.SCALE_MODE_FILL}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper mColorMode">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.COLOR_MODE_MONOCHROME, androidx.print.PrintHelper.COLOR_MODE_COLOR}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper mOrientation">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.ORIENTATION_LANDSCAPE, androidx.print.PrintHelper.ORIENTATION_PORTRAIT}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper mScaleMode">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.SCALE_MODE_FIT, androidx.print.PrintHelper.SCALE_MODE_FILL}" />
    </annotation>
  </item>
  <item name="androidx.print.PrintHelper void setColorMode(int) 0">
    <annotation name="androidx.annotation.IntDef">
      <val name="value" val="{androidx.print.PrintHelper.COLOR_MODE_MONOCHROME, androidx.print.PrintHelper.COLOR_MODE_COLOR}" />