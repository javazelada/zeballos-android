package com.qhatu.movil.conversionmonedas;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText bolivianos, dolaresEq, eurosEq, solesEq,
            chileEq, brasilEq, chinaEq, japonEq;
    TextView dolaresTc, eurosTc,
            solesTc, chileTc, brasilTc,
            chinaTc, japonTc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablaconversion);
        cargarTextView();
        tipoCambioBolivianos();
        inicializarValores();
    }

    public void cargarTextView() {
        bolivianos = (EditText) findViewById(R.id.edit_bolivianos);
        dolaresTc = (TextView) findViewById(R.id.edit_dolares_tc);
        dolaresEq = (EditText) findViewById(R.id.edit_dolares_eq);
        eurosTc = (TextView) findViewById(R.id.edit_euros_tc);
        eurosEq = (EditText) findViewById(R.id.edit_euros_eq);
        solesTc = (TextView) findViewById(R.id.edit_soles_tc);
        solesEq = (EditText) findViewById(R.id.edit_soles_eq);
        chileTc = (TextView) findViewById(R.id.edit_chile_tc);
        chileEq = (EditText) findViewById(R.id.edit_chile_eq);
        brasilTc = (TextView) findViewById(R.id.edit_brasil_tc);
        brasilEq = (EditText) findViewById(R.id.edit_brasil_eq);
        chinaTc = (TextView) findViewById(R.id.edit_china_tc);
        chinaEq = (EditText) findViewById(R.id.edit_china_eq);
        japonTc = (TextView) findViewById(R.id.edit_japon_tc);
        japonEq = (EditText) findViewById(R.id.edit_japon_eq);
    }

    public void tipoCambioBolivianos() {
        dolaresTc.setText("" + 6.96);
        eurosTc.setText("" + 7.52);
        solesTc.setText("" + 2.23);
        chileTc.setText("" + 0.011354);
        brasilTc.setText("" + 2.2852);
        chinaTc.setText("" + 1.13);
        japonTc.setText("" + 0.05849034);
    }

    public void inicializarValores() {
        bolivianos.setText("");
        dolaresEq.setText("");
        eurosEq.setText("");
        solesEq.setText("");
        chileEq.setText("");
        brasilEq.setText("");
        chinaEq.setText("");
        japonEq.setText("");
    }

    public void convertirMonedas(View vista) {
        if (!dolaresEq.getText().toString().isEmpty() && eurosEq.getText().toString().isEmpty() && solesEq.getText().toString().isEmpty()
                && solesEq.getText().toString().isEmpty() && chileEq.getText().toString().isEmpty()
                && brasilEq.getText().toString().isEmpty() && chileEq.getText().toString().isEmpty()
                && japonEq.getText().toString().isEmpty()
        ) {
            bolivianos.setText(String.format("%.2f", Double.parseDouble(dolaresEq.getText().toString()) * Double.parseDouble(dolaresTc.getText().toString())));
            convertirBolivianos();
        }
    }

    public void convertirBolivianos() {
        if (bolivianos.getText().toString().isEmpty())
            Toast.makeText(this, "Introdusca un valor para ser convertido.", Toast.LENGTH_SHORT).show();
        else {
            int montoBolivianos = Integer.parseInt(bolivianos.getText().toString());
            dolaresEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(dolaresTc.getText().toString())));
            eurosEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(eurosTc.getText().toString())));
            solesEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(solesTc.getText().toString())));
            chileEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(chileTc.getText().toString())));
            brasilEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(brasilTc.getText().toString())));
            chinaEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(chinaTc.getText().toString())));
            japonEq.setText(String.format("%.2f", montoBolivianos / Double.parseDouble(japonTc.getText().toString())));
        }
    }

    public void convertir(View vista) {
        convertirBolivianos();
    }

    public void inicializar(View vista) {
        inicializarValores();
    }


}
