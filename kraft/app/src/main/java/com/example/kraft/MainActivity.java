package com.example.kraft;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    int i = 0;
    int objetivo = 0;

    int dado[] = {R.drawable.and_uno, R.drawable.and_dos, R.drawable.and_tres,
            R.drawable.and_cuatro, R.drawable.and_cinco, R.drawable.and_seis};
    int juego[] = {R.drawable.and_cacho};

    Button btnlanzar, btnInicializar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imgD1 = (ImageView) findViewById(R.id.imagedado1);
        ImageView imgD2 = (ImageView) findViewById(R.id.imagedado2);
        imgD1.setVisibility(View.GONE);
        imgD2.setVisibility(View.GONE);
        btnlanzar = (Button) findViewById(R.id.btnlanzar);
        btnInicializar = (Button) findViewById(R.id.btnreiniciar);
        btnlanzar.setEnabled(true);
        btnInicializar.setEnabled(false);
    }

    public void lanzamiento(View vista) {
        ImageView cacho = (ImageView) findViewById(R.id.imageCacho);
        cacho.setVisibility(View.GONE);
        TextView pd1 = (TextView) findViewById(R.id.editText1);
        TextView pd2 = (TextView) findViewById(R.id.editText2);

        ImageView imgD1 = (ImageView) findViewById(R.id.imagedado1);
        ImageView imgD2 = (ImageView) findViewById(R.id.imagedado2);

        // valores entre 1 y 6
        int d1 = (int) (Math.random() * 6) + 1;
        int d2 = (int) (Math.random() * 6) + 1;

        imgD1.setImageResource(dado[d1 - 1]);
        imgD2.setImageResource(dado[d2 - 1]);
        imgD1.setVisibility(View.VISIBLE);
        imgD2.setVisibility(View.VISIBLE);

        evaluar(d1, d2);
        pd1.setText(d1 + "");
        pd2.setText(d2 + "");


    }

    public void inicializacion(View vista) {
        btnInicializar.setEnabled(false);
        btnlanzar.setEnabled(true);
        ImageView cacho = (ImageView) findViewById(R.id.imageCacho);
        cacho.setVisibility(View.VISIBLE);
        ImageView imgD1 = (ImageView) findViewById(R.id.imagedado1);
        ImageView imgD2 = (ImageView) findViewById(R.id.imagedado2);
        imgD1.setVisibility(View.GONE);
        imgD2.setVisibility(View.GONE);
        TextView pd1 = (TextView) findViewById(R.id.editText1);
        TextView pd2 = (TextView) findViewById(R.id.editText2);
        TextView res = (TextView) findViewById(R.id.resultado);
        TextView obj = (TextView) findViewById(R.id.objetivo);
        pd1.setText("");
        pd2.setText("");
        res.setText("");
        obj.setText("");
        i = 0;
    }

    public void evaluar(int d1, int d2) {
        TextView res = (TextView) findViewById(R.id.resultado);
        TextView obj = (TextView) findViewById(R.id.objetivo);

        int suma = d1 + d2;
        if (i == 0) {
            if (suma == 7 || suma == 11) {
                res.setText("Ganaste!");
                btnlanzar.setEnabled(false);
                btnInicializar.setEnabled(true);
            }
            if (suma == 2) {
                res.setText("Perdiste!");
                btnlanzar.setEnabled(false);
                btnInicializar.setEnabled(true);
            }

        }
        if (i == 0) {
            objetivo = suma;
            obj.setText("el objetivo es " + suma);
            i = 2;
        } else {
            if (i == 2) {
                if (suma == 7) {
                    res.setText("Perdiste!");
                    btnlanzar.setEnabled(false);
                    btnInicializar.setEnabled(true);
                } else {
                    if (suma == objetivo) {
                        res.setText("Ganaste!");
                        btnlanzar.setEnabled(false);
                        btnInicializar.setEnabled(true);

                    }
                }
            }
        }
    }
}

