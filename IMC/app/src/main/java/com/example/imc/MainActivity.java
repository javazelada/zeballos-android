package com.example.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View vista) {
        Intent nv = new Intent(this, IMCActivity.class);
        nv.putExtra("peso", 80);
        nv.putExtra("altura", 1.60);
        nv.putExtra("nombre", "Dario");
        startActivity(nv);
    }
}
