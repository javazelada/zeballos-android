package com.example.imc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class IMCActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);
        Bundle p = getIntent().getExtras();
        String qnombre = p.getString("nombre");
        Toast.makeText(this, "Llego "+ qnombre, 1).show();

    }

    public void retornar(View vista) {
        Intent r = new Intent(this, MainActivity.class);
        startActivity(r);
    }
}
