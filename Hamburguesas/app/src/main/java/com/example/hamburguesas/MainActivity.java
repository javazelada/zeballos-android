package com.example.hamburguesas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int hamburguesas[] = {R.drawable.hconfideitos, R.drawable.hconhuevo, R.drawable.hdoble, R.drawable.hgemelas, R.drawable.hmariajuana,
            R.drawable.hmediokilo, R.drawable.hnormal, R.drawable.hpollo, R.drawable.hvegetal
    };
    private String tituloPlatos[] = {"Hamburguesa con fideitos", "Hamburguesa con huevo", "Hamburguesa doble", "Hamburguesa gemela",
            "hamburguesa  mariajuana", "Hamburquesa medio kilo", "Hamburguesa normal", "Hamburguesa de pollo", "Hamburguesa vegetal"};
    final static private int precioPlato[] = {15, 22, 30, 32, 18, 25, 35, 20, 28};
    final static private int montoPlato[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    TextView montoPedido, montoPagar, tituloPlato;
    int indice = 0;
    int montoTotal = 0;

    ImageView imagePlatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imagePlatos = (ImageView) findViewById(R.id.imageHamburguesas);
        montoPedido = (TextView) findViewById(R.id.textPedido);
        montoPagar = (TextView) findViewById(R.id.textPagar);
        tituloPlato = (TextView) findViewById(R.id.tituloPlato);
        tituloPlato.setText(tituloPlatos[indice] +" "+ precioPlato[indice] + " Bs");
        actualizarDatos();
        mostrarImagen(indice);

    }

    public void mostrarImagen(int i) {
        imagePlatos.setImageResource(hamburguesas[i]);
    }

    public void anterior(View vista) {
        indice--;
        if (indice < 0)
            indice = hamburguesas.length - 1;
        mostrarImagen(indice);
        actualizarDatos();
    }

    public void siguiente(View vista) {
        indice++;
        if (indice > hamburguesas.length - 1)
            indice = 0;
        mostrarImagen(indice);
        actualizarDatos();
    }

    public void comprar(View vista) {
        montoPlato[indice]++;
        actualizarDatos();
    }

    public void cancelar(View vista) {
        for (int i = 0; i < montoPlato.length; i++) {
            montoPlato[i] = 0;
        }
        actualizarDatos();
    }

    public void detalle(View vista) {
        String hamburguesas = "hamburguesas         cantidad        total \n";
        for (int i = 0; i < montoPlato.length ; i++) {
            if (montoPlato[i] > 0) {
                hamburguesas = hamburguesas + "    "
                        + tituloPlatos[i] + "    "
                        + montoPlato[i]+  "      "
                        + montoPlato[i]*precioPlato[i] +"\n";
            }
        }
        hamburguesas = hamburguesas + "\nTotal a pagar: " +montoTotal;

        Intent nv = new Intent(this, FacturaActivity.class);
        nv.putExtra("factura", hamburguesas);
        nv.putExtra("monto_total", montoTotal);
        startActivity(nv);
//        LayoutInflater inflater = getLayoutInflater();
//        View layout = inflater.inflate(R.layout.custom_toast,
//                (ViewGroup) findViewById(R.id.custom_toast_container));
//
//        TextView text = (TextView) layout.findViewById(R.id.text);
//        text.setText(hamburguesas);
//
//        Toast toast = new Toast(getApplicationContext());
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
    }

    public void devolver(View vista) {
        if (montoPlato[indice] > 0)
            montoPlato[indice]--;
        actualizarDatos();
    }

    public void actualizarDatos() {
        montoTotal = 0;
        montoPedido.setText(montoPlato[indice] + "");
        tituloPlato.setText(tituloPlatos[indice] +" "+ precioPlato[indice] + " Bs");

        for (int i = 0; i < montoPlato.length; i++) {
            montoTotal = montoTotal + (montoPlato[i] * precioPlato[i]);
        }
        montoPagar.setText(montoTotal + "");
    }

    public void facturar(View view) {

    }

    public void fin(View view) {
        System.exit(0);
    }


}
