package com.example.hamburguesas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FacturaActivity extends AppCompatActivity {

    TextView facturaTexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura);
        facturaTexto = (TextView) findViewById(R.id.textFactura);
        Bundle p = getIntent().getExtras();
        String factura = p.getString("factura");
        facturaTexto.setText(factura);

    }

    public void retornar(View vista) {
        Intent r = new Intent(this, MainActivity.class);
        startActivity(r);
    }
}
