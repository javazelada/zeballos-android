package com.example.ventanas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PlatosEspecialesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_platos_especiales);
    }

    public void retornar(View vista) {
        Intent vd = new Intent(this, MainActivity.class);
        startActivity(vd);
    }
}
