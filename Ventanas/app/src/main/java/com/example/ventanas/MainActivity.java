package com.example.ventanas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void finalizar(View vista) {
        finish();
    }

    public void desayunos(View vista) {
        Intent vd = new Intent(this, DesayunoActivity.class);
        startActivity(vd);
    }

    public void bebidas(View vista) {
        Intent vd = new Intent(this, BebidasActivity.class);
        startActivity(vd);
    }

    public void almuerzos(View vista) {
        Intent vd = new Intent(this, AlmuerzosActivity.class);
        startActivity(vd);
    }

    public void platosEspeciales(View vista) {
        Intent vd = new Intent(this, PlatosEspecialesActivity.class);
        startActivity(vd);
    }

    public void postres(View vista) {
        Intent vd = new Intent(this, PostresActivity.class);
        startActivity(vd);
    }
}
