package com.example.registroventas1.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.registroventas1.entities.Precio;
import com.example.registroventas1.entities.Producto;

@Database(entities = {Precio.class, Producto.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PrecioDao precioDao();
    public abstract ProductoDao productoDao();
}