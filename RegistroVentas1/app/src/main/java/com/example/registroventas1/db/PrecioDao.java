package com.example.registroventas1.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.registroventas1.entities.Precio;

import java.util.List;

@Dao
public interface PrecioDao {
    @Query("SELECT * FROM precio")
    List<Precio> getAll();

    @Insert
    void insertAll(List<Precio> precios);

    @Delete
    void delete(Precio precio);

    @Query("SELECT MAX(fecha), codigo, precio, id FROM precio WHERE codigo = :codigo")
    Precio getSum(int codigo);
}
