package com.example.registroventas1.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import com.example.registroventas1.entities.Producto;

import java.util.List;

@Dao
public interface ProductoDao {
    @Query("SELECT * FROM producto")
    List<Producto> getAll();

    @Insert
    void insertAll(List<Producto> productos);

    @Delete
    void delete(Producto producto);

    @Query("SELECT * FROM producto WHERE codigo = :codigo")
    List<Producto> getById(int codigo);
}
