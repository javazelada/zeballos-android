package com.example.registroventas1.views;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import com.example.registroventas1.R;
import com.example.registroventas1.db.AppDatabase;
import com.example.registroventas1.db.PrecioDao;
import com.example.registroventas1.db.ProductoDao;
import com.example.registroventas1.entities.Precio;
import com.example.registroventas1.entities.Producto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static PrecioDao precioDao;
    public static ProductoDao productoDao;
    public static List<Producto> productos = new ArrayList<>();
    public static List<Precio> precios = new ArrayList<>();
    private Producto p;
    private Precio precio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializardb();
        leerArchivo();
    }

    public void consultaProducto(View view) {
        Intent intent = new Intent(this, ConsultaPrecioActivity.class);
        startActivity(intent);
    }


    public void inicializardb() {
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "listaPrecios-db")
//                .createFromAsset("database/listaPrecios-db")
                .allowMainThreadQueries()
                .build();
        db.clearAllTables();
        precioDao = db.precioDao();
        productoDao = db.productoDao();

    }

    public void leerArchivo() {
        String estado = Environment.getExternalStorageState();
        if (!estado.equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "No hay memoria SD", Toast.LENGTH_LONG).show();
            finish();
        }
        try {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            File dir = Environment.getExternalStorageDirectory();

            File pt = new File(dir.getAbsolutePath() + File.separator + "productos.csv");
            BufferedReader lee = new BufferedReader(new FileReader(pt));
            String linea, linea1;

            File pt1 = new File(dir.getAbsolutePath() + File.separator + "precios.csv");
            BufferedReader lee1 = new BufferedReader(new FileReader(pt1));

            while ((linea = lee.readLine()) != null) {
                p = new Producto();
                try {
                    p.setCodigo(Integer.parseInt(linea.split(";")[0]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                p.setProducto(linea.split(";")[1]);
                p.setUniven(linea.split(";")[2]);
                try {
                    p.setUniem(Integer.parseInt(linea.split(";")[3]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                try {
                    p.setLinea(Integer.parseInt(linea.split(";")[4]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                try {
                    p.setExitencia(Integer.parseInt(linea.split(";")[5]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                productos.add(p);

            }

            while ((linea1 = lee1.readLine()) != null) {
                precio = new Precio();
                try {
                    precio.setCodigo(Integer.parseInt(linea1.split(";")[0]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                try {
                    Date data1 = new SimpleDateFormat("dd/MM/yyyy").parse(linea1.split(";")[2]);
                    precio.setFecha(data1.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    precio.setPrecio(Double.parseDouble(linea1.split(";")[3]));
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                precios.add(precio);
            }

        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        productoDao.insertAll(productos);
        precioDao.insertAll(precios);

    }
}
