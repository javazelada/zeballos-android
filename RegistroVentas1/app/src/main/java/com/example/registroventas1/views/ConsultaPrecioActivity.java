package com.example.registroventas1.views;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.registroventas1.R;
import com.example.registroventas1.entities.Precio;
import com.example.registroventas1.entities.Producto;

import java.util.List;

import static com.example.registroventas1.views.MainActivity.precioDao;
import static com.example.registroventas1.views.MainActivity.productoDao;

public class ConsultaPrecioActivity extends AppCompatActivity {
    private EditText busqueda;
    private List<Producto> productos;
    private Precio precio;
    private TextView descripcion, precioVenta, univen, unixenv, codLin, existencia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_precio);
        busqueda = (EditText) findViewById(R.id.editCodigo);

        descripcion = (TextView) findViewById(R.id.textDescripcion);
        precioVenta = (TextView) findViewById(R.id.textPrecioVenta);
        univen = (TextView) findViewById(R.id.textUniVen);
        unixenv = (TextView) findViewById(R.id.textUnidxEnv);
        codLin = (TextView) findViewById(R.id.textCodLinea);
        existencia = (TextView) findViewById(R.id.textExistencia);
    }

    public void buscarProducto(View view) {
        int id = Integer.parseInt(busqueda.getText().toString());
        productos = productoDao.getById(id);
        precio = precioDao.getSum(id);
        Log.d("usuario", "------>>> "+ productos + "   " + productoDao.getAll().size());
        if (productos.size() > 0) {
            descripcion.setText(productos.get(0).getProducto());
            existencia.setText(productos.get(0).getExitencia() + "");
            unixenv.setText(productos.get(0).getUniem() + "");
            codLin.setText(productos.get(0).getLinea() + "");
            univen.setText(productos.get(0).getUniven());
        }
    }


}
