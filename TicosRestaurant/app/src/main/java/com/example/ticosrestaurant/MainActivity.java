package com.example.ticosrestaurant;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private int platos[] = {R.drawable.ajifideo, R.drawable.chairo, R.drawable.charquecan, R.drawable.fricase, R.drawable.milanesa,
            R.drawable.paceno, R.drawable.picantemixto, R.drawable.saice, R.drawable.sajta, R.drawable.sopafideo,
            R.drawable.sopamani, R.drawable.thimpu};
    private String tituloPlatos[] = {"Aji de fideo", "Chairo", "Charquecan", "Fricase", "Milanesa", "Plato Paceño", "Picante mixto",
            "Saice", "Sajta", "Sopa de fideo", "Sopa de Mani", "Thimpu"};
    private int precioPlato[] = {15, 22, 30, 32, 18, 25, 35, 20, 28, 12, 18, 26};
    private int montoPlato[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    TextView montoPedido, montoPagar, tituloPlato;
    int indice = 0;
    int montoTotal = 0;

    ImageView imagePlatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imagePlatos = (ImageView) findViewById(R.id.imagePlatos);
        montoPedido = (TextView) findViewById(R.id.textPedido);
        montoPagar = (TextView) findViewById(R.id.textPagar);
        tituloPlato = (TextView) findViewById(R.id.tituloPlato);
        mostrarImagen(indice);

    }

    public void mostrarImagen(int i) {
        imagePlatos.setImageResource(platos[i]);
    }

    public void anterior(View vista) {
        indice--;
        if (indice < 0)
            indice = 11;
        mostrarImagen(indice);
        actualizarDatos();
    }

    public void siguiente(View vista) {
        indice++;
        if (indice > 11)
            indice = 0;
        mostrarImagen(indice);
        actualizarDatos();
    }

    public void comprar(View vista) {
        montoPlato[indice]++;
        actualizarDatos();
    }

    public void cancelar(View vista) {
        for (int i = 0; i < montoPlato.length; i++) {
            montoPlato[i] = 0;
        }
        actualizarDatos();
    }

    public void detalle(View vista) {
        String platos = "platos         cantidad \n";
        for (int i = 0; i < montoPlato.length ; i++) {
            if (montoPlato[i] > 0) {
                platos = platos + "    " + tituloPlatos[i] + "    " + montoPlato[i]+ "\n";
            }
        }
        platos = platos + "\nTotal a pagar: " +montoTotal;

//        Toast.makeText(this, "asdfasdf  \n lksjdkfljsd", Toast.LENGTH_LONG).show();
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(platos);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void devolver(View vista) {
        if (montoPlato[indice] > 0)
            montoPlato[indice]--;
        actualizarDatos();
    }

    public void actualizarDatos() {
        montoTotal = 0;
        montoPedido.setText(montoPlato[indice] + "");
        tituloPlato.setText(tituloPlatos[indice]);
        for (int i = 0; i < montoPlato.length; i++) {
            montoTotal = montoTotal + (montoPlato[i] * precioPlato[i]);
        }
        montoPagar.setText(montoTotal + "");
    }

    public static String convertArrayToString(int[] strArray) {
        return Arrays.toString(strArray);
    }


}
