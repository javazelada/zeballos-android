package com.example.horoscopo;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager mSensorManager = null;
    private Sensor mAccelerometer = null;
    private float lastX,lastY,lastZ;


    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;

    int imagenes[] = {R.drawable.aries, R.drawable.tauro, R.drawable.geminis, R.drawable.cancer, R.drawable.leo, R.drawable.virgo,
            R.drawable.libra, R.drawable.escorpion, R.drawable.sagitario, R.drawable.capricornio, R.drawable.acuario, R.drawable.piscis};
    String titulos[] = {"aries", "tauro", "geminis", "cancer", "leo", "virgo", "libra", "escorpion", "sagitario", "capricornio", "acuario", "piscis"};
    int indice = 0;
    TextView titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titulo = (TextView) findViewById(R.id.titulo);
        titulo.setText(titulos[0]);
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void siguiente(View vista) {
        indice ++;
        if (indice > 11)
            indice = 0;
        ImageView pImg = (ImageView) findViewById(R.id.imageView1);
        pImg.setImageResource(imagenes[indice]);
        titulo.setText(titulos[indice]);
    }

    public void anterior(View vista) {
        indice --;
        if (indice < 0)
            indice = 11;
        ImageView pImg = (ImageView) findViewById(R.id.imageView1);
        pImg.setImageResource(imagenes[indice]);
        titulo.setText(titulos[indice]);

    }

    public void ocultar() {

    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d("sensor-app", "eventosensor--" + event);

        deltaX = Math.abs(lastX - event.values[0]);
        deltaY = Math.abs(lastY - event.values[1]);
        deltaZ= Math.abs(lastZ- event.values[2]);

        if (deltaX > 5){
            // izquierda
//            maxX.setText("Left");
            indice --;
            if (indice < 0)
                indice = 11;
            ImageView pImg = (ImageView) findViewById(R.id.imageView1);
            pImg.setImageResource(imagenes[indice]);
            titulo.setText(titulos[indice]);

        }
        else if(deltaX < -5){
            //derecha
            indice ++;
            if (indice > 11)
                indice = 0;
            ImageView pImg = (ImageView) findViewById(R.id.imageView1);
            pImg.setImageResource(imagenes[indice]);
            titulo.setText(titulos[indice]);
//            maxX.setText("Right");
        }
//        if (deltaY > 0){
//            //
//            maxY.setText("Down");
//        }
//        else if(deltaY < 0){
//            maxY.setText("Up");
//        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        Log.d("sensor-app", sensor + "  " + i);
    }
}
